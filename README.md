# Pipe & Filter implementation

Supports running three systems, all following the Pipe & Filter architecture

* System A: Converts temperature and altitude readings into the metric system
* System B: Identifies and extracts "wild points" in the pressure readings in addition to performing all the tasks that system A offers
* System C: Merges two data streams, filters all data collected below 10k feet altitude and filters "wild points"

### Prerequisites

Install all of the following software:
* Git
* Java >= 1.8

### How to run

Build the project by using the provided Maven wrapper.
The generated _fat-jar_ can then be found in the `target` directory.

1. Clone project and change into directory

        git clone git@gitlab.informatik.hu-berlin.de:meinerfq/resa-ex3.git

2. Build and run

    * Linux
        
            ./mvnw clean package
            java -jar target/Ex03_Joker_SS2018.jar <system_id>
    
    * Windows
    
            mvnw.cmd clean package
            java -jar target/Ex03_Joker_SS2018.jar <system_id>

Valid `<system_id>` values are `a`, `b` and `c`.