/******************************************************************************************************************
 * File:WildpointFilter.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for how to use the FilterRemplate to create a standard filter. This particular
 * example is a simple "pass-through" filter that reads data from the filter's input port and writes data out the
 * filter's output port.
 *
 * Parameters: 		None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/

package pipe_filter.system_c;

import org.apache.commons.lang3.ArrayUtils;
import pipe_filter.common.FilterUtil;
import pipe_filter.common.Frame;
import pipe_filter.common.EndOfStreamException;
import pipe_filter.common.FilterFramework;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class TenKFilter extends FilterFramework {

    private static final String TEN_K_FILE = "LessThan10K.dat";
    private static final String TEN_K_HEADER = "Time:\t Altitude:\n";

    private final List<Frame> frames = new ArrayList<>();

    // assembles Frame object from incoming bytes, converts values, and sends the *Frame* object as byte stream to the sink
    public void run() {

        FilterUtil.writeLineToFile(TEN_K_FILE, TEN_K_HEADER, true);

        List<Byte> buffer = new ArrayList<>();
        Frame frame;

        // collects bytes until a new object can be deserialized
        while (true) {
            try {
                buffer.add(readFilterInputPort());

                Byte[] bytes = buffer.toArray(new Byte[0]);

                try (ByteArrayInputStream bis = new ByteArrayInputStream(ArrayUtils.toPrimitive(bytes))){

                    // once a new Frame object can be deserialized, clear the byte buffer
                    ObjectInputStream in = new ObjectInputStream(bis);
                    frame = (Frame) in.readObject();
                    frames.add(frame);

                    buffer.clear();

                } catch (IOException e) {
                    // only occurs when no bytes are read
                    // output would clutter stdout
                    // FIXME empty catch block?
                } catch (ClassNotFoundException e) {
                    // wrong assumption: merely indicates that not enough bytes have been received to deserialize frame object
                    // FIXME when is this exception thrown?
                    System.out.println("Failed to deserialize frame: " + e.getMessage());
                }
            } catch (EndOfStreamException e) {
                filter10kFrames();
                closePorts();
                break;
            }
        }
    }

    private void filter10kFrames() {
        FilterUtil.writeLineToFile(TEN_K_FILE, TEN_K_HEADER, true);

        frames.forEach(frame -> {
            if (frame.getAltitude() > 10000) {
                writeFrameToOutput(frame);
            } else {
                writeLessThan10kFrameToFile(frame);
            }
        });
    }

    private void writeLessThan10kFrameToFile(Frame frame) {
        String formattedFrame = FilterUtil.formatTime(frame.getTime()) + "\t"
                + FilterUtil.formatAltitude(frame.getAltitude()) + "\n";

        FilterUtil.writeLineToFile(TEN_K_FILE, formattedFrame, false);
    }
}