/******************************************************************************************************************
 * File:SourceFilterA.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for how to use the SourceFilterTemplate to create a source filter. This particular
 * filter is a source filter that reads some input from the FlightData.dat file and writes the bytes up stream.
 *
 * Parameters: 		None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/

package pipe_filter.system_c;

import pipe_filter.common.FilterFramework;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

public class SourceFilterC extends FilterFramework {

    private static final String INPUT_FILE_A = "SubSetA.dat";    // Input data file.
    private static final String INPUT_FILE_B = "SubSetB.dat";    // Input data file.

    private ClassLoader classLoader;

    private int bytesread = 0;                    // Number of bytes read from the input file.
    private int byteswritten = 0;                // Number of bytes written to the stream.

    public void run() {
        classLoader = Thread.currentThread().getContextClassLoader();

        readFile(INPUT_FILE_A);
        readFile(INPUT_FILE_B);

        closePorts();
    }

    private void readFile(String file) {
        try (DataInputStream in = new DataInputStream(classLoader.getResourceAsStream(file))) {

            System.out.println("\n" + this.getName() + "::Source reading file...");

            while (true) {
                byte databyte = in.readByte();
                bytesread++;
                writeFilterOutputPort(databyte);
                byteswritten++;
            }
        } catch (EOFException eoferr) {
            System.out.println("\n" + this.getName() + "::End of file reached...");
        } catch (IOException iox) {
            System.out.println("\n" + this.getName() + "::Problem reading input data file::" + iox);

        }
    }
}