/******************************************************************************************************************
 * File:WildpointFilter.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for how to use the FilterRemplate to create a standard filter. This particular
 * example is a simple "pass-through" filter that reads data from the filter's input port and writes data out the
 * filter's output port.
 *
 * Parameters: 		None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/

package pipe_filter.system_c;

import org.apache.commons.lang3.ArrayUtils;
import pipe_filter.common.FilterUtil;
import pipe_filter.common.Frame;
import pipe_filter.common.EndOfStreamException;
import pipe_filter.common.FilterFramework;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class SortFilter extends FilterFramework {

    private static final String WILDPOINT_FILE = "WildPoints.dat";
    private static final String WILDPOINT_HEADER = "Time:\tPressure (psi):\n";

    private final List<Frame> frames = new ArrayList<>();

    // assembles Frame object from incoming bytes, converts values, and sends the *Frame* object as byte stream to the sink
    public void run() {

        FilterUtil.writeLineToFile(WILDPOINT_FILE, WILDPOINT_HEADER, true);

        List<Byte> buffer = new ArrayList<>();
        Frame frame;

        // collects bytes until a new object can be deserialized
        while (true) {
            try {
                buffer.add(readFilterInputPort());

                Byte[] bytes = buffer.toArray(new Byte[0]);

                try (ByteArrayInputStream bis = new ByteArrayInputStream(ArrayUtils.toPrimitive(bytes))){

                    // once a new Frame object can be deserialized, clear the byte buffer
                    ObjectInputStream in = new ObjectInputStream(bis);
                    frame = (Frame) in.readObject();
                    frames.add(frame);

                    buffer.clear();

                } catch (IOException e) {
                    // only occurs when no bytes are read
                    // output would clutter stdout
                    // FIXME empty catch block?
                } catch (ClassNotFoundException e) {
                    // wrong assumption: merely indicates that not enough bytes have been received to deserialize frame object
                    // FIXME when is this exception thrown?
                    System.out.println("Failed to deserialize frame: " + e.getMessage());
                }
            } catch (EndOfStreamException e) {
                sortFramesByTimeAsc();
                frames.forEach(this::writeFrameToOutput);
                closePorts();
                break;
            }
        }
    }

    private void sortFramesByTimeAsc() {
        frames.sort((frame1, frame2)
                -> (int) (frame1.getTime() - frame2.getTime()));
    }
}