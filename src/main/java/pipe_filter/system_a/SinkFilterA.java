/******************************************************************************************************************
 * File:SinkFilterA.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for using the SinkFilterTemplate for creating a sink filter. This particular
 * filter reads some input from the filter's input port and does the following:
 *
 *	1) It parses the input stream and "decommutates" the measurement ID
 *	2) It parses the input steam for measurments and "decommutates" measurements, storing the bits in a long word.
 *
 * This filter illustrates how to convert the byte stream data from the upstream filterinto useable data found in
 * the stream: namely time (long type) and measurements (double type).
 *
 *
 * Parameters: 	None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/
package pipe_filter.system_a;

import org.apache.commons.lang3.ArrayUtils;
import pipe_filter.common.FilterUtil;
import pipe_filter.common.Frame;
import pipe_filter.common.EndOfStreamException;
import pipe_filter.common.FilterFramework;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;                        // This class is used to interpret time words


public class SinkFilterA extends FilterFramework {

    private static final String outputFile = "OutputA.dat";
    private static final String header = "Time:\tTemperature (C):\tAltitude (m):\n";

    public void run() {

        FilterUtil.writeLineToFile(outputFile, header, true);

        List<Byte> buffer = new ArrayList<>();
        Frame frame;

        // collects bytes until a new object can be deserialized
        while (true) {
            try {
                buffer.add(readFilterInputPort());

                Byte[] bytes = buffer.toArray(new Byte[0]);

                try (ByteArrayInputStream bis = new ByteArrayInputStream(ArrayUtils.toPrimitive(bytes))){

                    // once a new Frame object can be deserialized, clear the byte buffer
                    ObjectInputStream in = new ObjectInputStream(bis);
                    frame = (Frame) in.readObject();

                    String formattedFrame = FilterUtil.formatTime(frame.getTime()) + "\t"
                            + FilterUtil.formatTemperature(frame.getTemperature()) + "\t"
                            + FilterUtil.formatAltitude(frame.getAltitude()) + "\n";

                    FilterUtil.writeLineToFile(outputFile, formattedFrame, false);

                    buffer.clear();

                } catch (IOException e) {
                    // only occurs when no bytes are read
                    // output would clutter stdout
                    // FIXME empty catch block?
                } catch (ClassNotFoundException e) {
                    // wrong assumption: merely indicates that not enough bytes have been received to deserialize frame object
                    // FIXME when is this exception thrown?
                    System.out.println("Failed to deserialize frame: " + e.getMessage());
                }
            } catch (EndOfStreamException e) {
                closePorts();
                break;
            }
        }
    }
}