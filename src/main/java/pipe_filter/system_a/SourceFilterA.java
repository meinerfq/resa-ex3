/******************************************************************************************************************
 * File:SourceFilterA.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for how to use the SourceFilterTemplate to create a source filter. This particular
 * filter is a source filter that reads some input from the FlightData.dat file and writes the bytes up stream.
 *
 * Parameters: 		None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/

package pipe_filter.system_a;

import pipe_filter.common.FilterFramework;

import java.io.*; // note we must add this here since we use BufferedReader class to read from the keyboard

public class SourceFilterA extends FilterFramework {

    private static final String INPUT_FILE = "FlightData.dat";    // Input data file.

    public void run() {

        int bytesread = 0;                    // Number of bytes read from the input file.
        int byteswritten = 0;                // Number of bytes written to the stream.
        DataInputStream in = null;            // File stream reference.
        byte databyte = 0;                    // The byte of data read from the file

        try {

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            in = new DataInputStream(classLoader.getResourceAsStream(INPUT_FILE));

            System.out.println("\n" + this.getName() + "::Source reading file...");

            while (true) {
                databyte = in.readByte();
                bytesread++;
                writeFilterOutputPort(databyte);
                byteswritten++;
            }
        } catch (EOFException eoferr) {
            System.out.println("\n" + this.getName() + "::End of file reached...");
            try {
                in.close();
                closePorts();
                System.out.println("\n" + this.getName() + "::Read file complete, bytes read::" + bytesread + " bytes written: " + byteswritten);

            } catch (Exception closeerr) {
                System.out.println("\n" + this.getName() + "::Problem closing input data file::" + closeerr);

            }
        } catch (IOException iox) {
            System.out.println("\n" + this.getName() + "::Problem reading input data file::" + iox);

        }
    }
}