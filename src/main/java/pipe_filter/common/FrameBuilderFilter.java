/******************************************************************************************************************
 * File:WildpointFilter.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for how to use the FilterRemplate to create a standard filter. This particular
 * example is a simple "pass-through" filter that reads data from the filter's input port and writes data out the
 * filter's output port.
 *
 * Parameters: 		None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/

package pipe_filter.common;

public class FrameBuilderFilter extends FilterFramework {

    private static final int MEASUREMENT_LENGTH = 8;        // This is the length of all measurements (including time) in bytes
    private static final int ID_LENGTH = 4;                // This is the length of IDs in the byte stream


    // assembles Frame object from incoming bytes, converts values, and sends the *Frame* object as byte stream to the sink
    public void run() {

        Frame currentFrame = null;
        while (true) {
            try {
                long id = readBytes(ID_LENGTH);
                long measurement = readBytes(MEASUREMENT_LENGTH);

                if (id == 0) {
                    // frame is "finished" once a new one begins
                    if (currentFrame != null) {
                        System.out.println(currentFrame.toString());
                        writeFrameToOutput(currentFrame);
                    }

                    // new frame
                    currentFrame = new Frame(measurement);
                }

                if (id == 1) {
                    if (currentFrame != null) {
                        currentFrame.setVelocity(Double.longBitsToDouble(measurement));
                    } else {
                        // should never happen
                        System.out.println("Received altitude measurement outside of frame");
                    }
                }

                if (id == 2) {
                    if (currentFrame != null) {
                        currentFrame.setAltitude(Double.longBitsToDouble(measurement));
                    } else {
                        // should never happen
                        System.out.println("Received altitude measurement outside of frame");
                    }
                }

                if (id == 3) {
                    if (currentFrame != null) {
                        currentFrame.setPressure(Double.longBitsToDouble(measurement));
                    } else {
                        // should never happen
                        System.out.println("Received altitude measurement outside of frame");
                    }
                }

                if (id == 4) {
                    if (currentFrame != null) {
                        currentFrame.setTemperature(Double.longBitsToDouble(measurement));
                    } else {
                        // should never happen
                        System.out.println("Received temperature measurement outside of frame");
                    }
                }

                if (id == 5) {
                    if (currentFrame != null) {
                        currentFrame.setAttitude(Double.longBitsToDouble(measurement));
                    } else {
                        // should never happen
                        System.out.println("Received temperature measurement outside of frame");
                    }
                }
            } catch (EndOfStreamException e) {
                closePorts();
                System.out.print("\n" + this.getName() + "::Sink Exiting");
                break;
            }
        }
    }
}