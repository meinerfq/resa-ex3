package pipe_filter.common;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class FilterUtil {

    public static void writeLineToFile(String file, String line, boolean newFile) {

        // write result to file, append if no new file is requested
        try (FileWriter writer = new FileWriter(file, !newFile)) {
            writer.write(line);
        } catch (IOException e) {
            System.out.println("Failed to write result to file: " + e.getMessage());
        }
    }

    public static String formatTime(Long time) {
        return new SimpleDateFormat("yyyy:dd:hh:mm:ss").format(time);
    }

    public static Double temperatureToCelsius(Double temperatureInFahrenheit) {
        return (temperatureInFahrenheit - 32) * (5.0/9);
    }

    public static Double altitudeToMeters(Double altitudeInFeet) {
        return altitudeInFeet / 3.2808;
    }

    public static String formatTemperature(Double temperature) {
        return String.format("%08.5f", temperature);
    }

    public static String formatAltitude(Double altitude) {
        return String.format("%11.5f", altitude);
    }

    public static String formatPressure(Double pressure) {
        return String.format("%07.5f", pressure);
    }
}