package pipe_filter.common;

public class EndOfStreamException extends Exception {

    static final long serialVersionUID = 0; // the version for streaming

    public EndOfStreamException () { super(); }

    public EndOfStreamException(String s) { super(s); }
}