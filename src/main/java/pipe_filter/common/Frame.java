package pipe_filter.common;

import java.io.Serializable;

public class Frame implements Serializable {

    private Long time;
    private Double velocity;
    private Double altitude;
    private Double pressure;
    private Double temperature;
    private Double attitude;
    private Boolean isPressureWildPoint;

    public Frame(long time) {
        this.time = time;
        this.isPressureWildPoint = false;
    }

    //Copy constructor.
    public Frame(Frame frame) {
        this.time = frame.getTime();
        this.velocity = frame.getVelocity();
        this.altitude = frame.getAltitude();
        this.pressure = frame.getPressure();
        this.temperature = frame.getTemperature();
        this.attitude = frame.getAttitude();
        this.isPressureWildPoint = frame.isPressureWildPoint();
    }

    public Long getTime() {
        return time;
    }

    public Double getVelocity() {
        return velocity;
    }

    public void setVelocity(Double velocity) {
        this.velocity = velocity;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getAttitude() {
        return attitude;
    }

    public void setAttitude(Double attitude) {
        this.attitude = attitude;
    }

    public Boolean isPressureWildPoint() {
        return isPressureWildPoint;
    }

    public void setPressureWildPoint(Boolean isPressureWildPoint) {
        this.isPressureWildPoint = isPressureWildPoint;
    }

    @Override
    public String toString() {
        return "Frame{" +
                "time=" + time +
                ", velocity=" + velocity +
                ", altitude=" + altitude +
                ", pressure=" + pressure +
                ", temperature=" + temperature +
                ", attitude=" + attitude +
                ", isPressureWildPoint=" + isPressureWildPoint +
                '}';
    }
}
