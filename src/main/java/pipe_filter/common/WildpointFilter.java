/******************************************************************************************************************
 * File:WildpointFilter.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Sample Pipe and Filter code (ajl).
 *
 * Description:
 *
 * This class serves as an example for how to use the FilterRemplate to create a standard filter. This particular
 * example is a simple "pass-through" filter that reads data from the filter's input port and writes data out the
 * filter's output port.
 *
 * Parameters: 		None
 *
 * Internal Methods: None
 *
 ******************************************************************************************************************/

package pipe_filter.common;

import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WildpointFilter extends FilterFramework {

    private static final String WILDPOINT_HEADER = "Time:\tPressure (psi):\n";

    private final List<Frame> frames = new ArrayList<>();
    private String wildPointFileName;

    public WildpointFilter(String wildPointfileName) {
        this.wildPointFileName = wildPointfileName;
    }

    // assembles Frame object from incoming bytes, converts values, and sends the *Frame* object as byte stream to the sink
    public void run() {

        FilterUtil.writeLineToFile(wildPointFileName, WILDPOINT_HEADER, true);

        List<Byte> buffer = new ArrayList<>();
        Frame frame;

        // collects bytes until a new object can be deserialized
        while (true) {
            try {
                buffer.add(readFilterInputPort());

                Byte[] bytes = buffer.toArray(new Byte[0]);

                try (ByteArrayInputStream bis = new ByteArrayInputStream(ArrayUtils.toPrimitive(bytes))){

                    // once a new Frame object can be deserialized, clear the byte buffer
                    ObjectInputStream in = new ObjectInputStream(bis);
                    frame = (Frame) in.readObject();
                    frames.add(frame);

                    buffer.clear();

                } catch (IOException e) {
                    // only occurs when no bytes are read
                    // output would clutter stdout
                    // FIXME empty catch block?
                } catch (ClassNotFoundException e) {
                    // wrong assumption: merely indicates that not enough bytes have been received to deserialize frame object
                    // FIXME when is this exception thrown?
                    System.out.println("Failed to deserialize frame: " + e.getMessage());
                }
            } catch (EndOfStreamException e) {
                handleFrames();
                closePorts();
                break;
            }
        }
    }

    private void handleFrames() {

        detectWildPoints();

        for (int frameIndex = 0; frameIndex < frames.size(); frameIndex++) {

            Frame frame = frames.get(frameIndex);

            // extrapolate wildpoints
            if (frame.isPressureWildPoint()) {
                writeWildPointToFile(frame);

                Double previousValidPressure = getPreviousValidPressureReading(frameIndex);
                Double nextValidPressure = getNextValidPressureReading(frameIndex);

                Double extrapolatedPressure = nextValidPressure != null
                        ? (previousValidPressure + nextValidPressure) / 2
                        : previousValidPressure;

                frame.setPressure(extrapolatedPressure);
            }

            writeFrameToOutput(frame);
        }
    }

    private void detectWildPoints() {
        for (int frameIndex = 0; frameIndex < frames.size(); frameIndex++) {
            Frame currentFrame = frames.get(frameIndex);

            if (currentFrame.getPressure() < 0.0) {
                currentFrame.setPressureWildPoint(true);
            } else {
                Double lastValidReading = getPreviousValidPressureReading(frameIndex);

                Boolean isWildPoint = lastValidReading != null
                        && Math.abs(lastValidReading - currentFrame.getPressure()) > 10;

                currentFrame.setPressureWildPoint(isWildPoint);
            }
        }
    }

    private Double getPreviousValidPressureReading(int frameIndex) {
        Double previousValidReading = null;

        int previousIndex = frameIndex;
        while (true) {
            previousIndex--;

            if (previousIndex < 0)
                break;

            Frame previousFrame = frames.get(previousIndex);

            if (!previousFrame.isPressureWildPoint()) {
                previousValidReading = previousFrame.getPressure();
                break;
            }
        }

        return previousValidReading;
    }

    private Double getNextValidPressureReading(int frameIndex) {
        Double nextValidReading = null;

        int nextIndex = frameIndex;
        while (true) {
            nextIndex++;

            if (nextIndex >= frames.size())
                break;

            Frame nextFrame = frames.get(nextIndex);

            if (!nextFrame.isPressureWildPoint()) {
                nextValidReading = nextFrame.getPressure();
                break;
            }
        }

        return nextValidReading;
    }

    private void writeWildPointToFile(Frame frame) {
        // format and write wildpoint to file
        String wildPoint = FilterUtil.formatTime(frame.getTime()) + "\t"
                + FilterUtil.formatPressure(frame.getPressure()) + "\n";

        FilterUtil.writeLineToFile(wildPointFileName, wildPoint, false);
    }
}