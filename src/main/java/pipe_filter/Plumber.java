package pipe_filter;

import pipe_filter.common.ConverterFilter;
import pipe_filter.common.FrameBuilderFilter;
import pipe_filter.system_a.SinkFilterA;
import pipe_filter.system_a.SourceFilterA;
import pipe_filter.common.SinkFilter;
import pipe_filter.common.WildpointFilter;
import pipe_filter.system_c.SortFilter;
import pipe_filter.system_c.SourceFilterC;
import pipe_filter.system_c.TenKFilter;

/******************************************************************************************************************
 * File:pipe_filter.Plumber.java
 * Course: 17655
 * Project: Assignment 1
 * Copyright: Copyright (c) 2003 Carnegie Mellon University
 * Versions:
 *	1.0 November 2008 - Initial rewrite of original assignment 1 (ajl).
 *
 * Description:
 *
 * This class serves as a template for creating a main thread that will instantiate and connect a set of filters.
 * The details of the filter operations is totally self contained within the filters, the "plumber" takes care of
 * starting the filters and connecting each of the filters together. The details of how to connect filters is taken
 * care of by the FilterFramework. In order to use this template the program should rename the class.
 * The template includes the RunFilter() method which is executed when the filter is started. While simple, there
 * are semantics for instantiating, connecting, and starting the filters:
 *
 *	Step 1: Instantiate the filters as shown in the example below. You should create the filters using the
 *			pipe_filter.templates provided, and you must use the FilterFramework as a base class for all filters. Every pipe and
 *			filter network must have a source filter where data originates, and a sink filter where the data flow
 *			terminates.
 *
 *	Step 2: connect the filters. Start with the sink and work backward to the source. Essentially you are connecting
 *			the	input of each filter to the up-stream filter's output until you get to the source filter. Filter have
 *			a connect() method which accepts a FilterFramework type. This method connects the calling filter's input
 *			to the passed filter's output. Again the example in the comments below illustrates how this is done.
 *
 *	Step 3: Start the filters using the start() method.
 *
 *	Once the filters are started this main thread dies and the pipe and filter network processes data until there
 * 	is no more data movement from the source. Each filter will shutdown when data is no longer available (provided
 *	you followe the read semantics described in the filter pipe_filter.templates).
 *	on their input ports.
 *
 * Parameters: 		None
 *
 * Internal Methods:	None
 *
 ******************************************************************************************************************/

public class Plumber {
    public static void main(String argv[]) {

        if (argv.length == 1) {
            if ("a".equals(argv[0].toLowerCase())) {
                System.out.println("Running System A...");

                SourceFilterA sourceFilterA = new SourceFilterA();    // This is a source filter - see SourceFilterTemplate.java
                FrameBuilderFilter frameBuilderFilter = new FrameBuilderFilter();    // This is a standard filter - see FilterTemplate.java
                ConverterFilter converterFilter = new ConverterFilter();
                SinkFilterA sinkFilterA = new SinkFilterA();        // This is a sink filter - see SinkFilterTemplate.java

                sinkFilterA.connect(converterFilter); // This essentially says, "connect sinkFilterA's input port to frameBuilderFilter's output port
                converterFilter.connect(frameBuilderFilter);
                frameBuilderFilter.connect(sourceFilterA); // This essentially says, "connect frameBuilderFilter's input port to sourceFilterA's output port

                sourceFilterA.start();
                frameBuilderFilter.start();
                converterFilter.start();
                sinkFilterA.start();
            } else if ("b".equals(argv[0].toLowerCase())) {
                System.out.println("Running System B...");

                SourceFilterA sourceFilterA = new SourceFilterA();    // This is a source filter - see SourceFilterTemplate.java
                FrameBuilderFilter frameBuilderFilter = new FrameBuilderFilter();
                ConverterFilter converterFilter = new ConverterFilter();
                WildpointFilter wildpointFilter = new WildpointFilter("WildPoints.dat");    // This is a standard filter - see FilterTemplate.java
                SinkFilter sinkFilter = new SinkFilter("OutputB.dat");        // This is a sink filter - see SinkFilterTemplate.java

                sinkFilter.connect(wildpointFilter); // This essentially says, "connect sinkFilter's input port to wildpointFilter's output port
                wildpointFilter.connect(converterFilter); // This essentially says, "connect wildpointFilter's input port to sourceFilterA's output port
                converterFilter.connect(frameBuilderFilter);
                frameBuilderFilter.connect(sourceFilterA);

                sourceFilterA.start();
                frameBuilderFilter.start();
                converterFilter.start();
                wildpointFilter.start();
                sinkFilter.start();
            } else if ("c".equals(argv[0].toLowerCase())) {
                System.out.println("Running System C...");

                SourceFilterC sourceFilterC = new SourceFilterC();
                FrameBuilderFilter frameBuilderFilter = new FrameBuilderFilter();
                SortFilter sortFilter = new SortFilter();
                TenKFilter tenKFilter = new TenKFilter();
                ConverterFilter converterFilter = new ConverterFilter();
                WildpointFilter wildpointFilter = new WildpointFilter("PressureWildPoints.dat");
                SinkFilter sinkFilter = new SinkFilter("OutputC.dat");

                sinkFilter.connect(wildpointFilter);
                wildpointFilter.connect(converterFilter);
                converterFilter.connect(tenKFilter);
                tenKFilter.connect(sortFilter);
                sortFilter.connect(frameBuilderFilter);
                frameBuilderFilter.connect(sourceFilterC);

                sourceFilterC.start();
                frameBuilderFilter.start();
                sortFilter.start();
                tenKFilter.start();
                converterFilter.start();
                wildpointFilter.start();
                sinkFilter.start();
            } else {
                System.out.println("Invalid system specified ('a', 'b', 'c')");
            }
        } else {
            System.out.println("Invalid number of arguments. Specify one type of system you'd like to run ('a', 'b', 'c')");
        }
    }
}